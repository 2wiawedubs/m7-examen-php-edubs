<?php
include('Class/ApunteClass.php');
define("UPLOAD_FOLDER", "assets/uploads/");

class Lista {
    private $_db = '';
    private $_list = [];
    private $test = "";
    /*
    *   Define el arhcivo de base de datos e inicializa el proceso de lectura: load()
    *   __construct($db)
    *   PARAMS: 
    *       $db : Nombre del archivo de base de datos
    */
    function __construct($db) {
        $this->test = $this->test . $_SERVER['DOCUMENT_ROOT'] . '/' . UPLOAD_FOLDER . $db . '.db';
        $this->_db = $_SERVER['DOCUMENT_ROOT'] . '/' . UPLOAD_FOLDER . $db . '.db';
        if (is_file($this->_db))
            $this->load();
    }

    /*
    *   -Abre el archivo de base de datos elegido obteniendo los datos de cada linea
    *   -Para cada uno de los registros en la base de datos crea un objeto de tipo  APUNTE con el titulo y la url (src) del archiv
    *   -Los APUNTES se guardan en el array _list
    *   -Ha de comprobar que el documento es de tipo FILE a traves del metodo is_doc
    *   load()
    *   PARAMS: none
    */
    public function load() {
        $dbFile = fopen($this->_db, "r");
        while (!feof($dbFile)) {
            $this->test = $this->test . "hdfisf";
            $line = trim(fgets($dbFile)); // Gets the next line in the $dbFile
            $this->test = $this->test . $line . "<br>";
            if ($line == null) break;
            $lineData = explode("###", $line); // Gets the title and the source (url) of the line
            if (/*$lineData[0] != "" && file_exists($lineData[1]) && */$this->is_doc($lineData[1])) 
                array_push($this->_list, new Apunte($lineData[0], $lineData[1]));
        }
        fclose($dbFile);
    }
    /*
    *   Deuelve la lista de apuntes cargados de la base de datos
    */
    public function get() {
        return $this->_list;
    }
    /*
    *   Devuelve true si el archivo es de tipo FILE
    */
    public function is_doc($path) {
        $info = filetype($path);
        $allowed = array("file");
        if (in_array($info, $allowed)) {
            return true;
        }
        return false;
    }
    function print() {
        return $this->test;
    }
}
