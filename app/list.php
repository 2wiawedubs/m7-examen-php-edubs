<?php
include_once('_header.php');
include("Class/ListaClass.php")
?>
<div class="row">
    <?php 
        $list = new Lista($_GET["subject"]);
        
    ?>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Documento</th>
            </tr>
        </thead>
        <tbody>
            <?php

            for ($i=0; $i < sizeof($list->get()); $i++) { 
                ?>
                <tr>
                    <td>0</td>
                    <td><?= $list->get()[$i]->title() ?></td>
                    <td><?= $list->get()[$i]->fileName() ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php include_once('_footer.php') ?>